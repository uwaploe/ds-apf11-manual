(TeX-add-style-hook
 "IridiumApex"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "10pt")))
   (TeX-run-style-hooks
    "latex2e"
    "FwRev"
    "Intro"
    "ProfileCycleModel"
    "MissionConfiguration"
    "RemoteControl"
    "RecoveryMode"
    "Data"
    "RemoteHost"
    "IridiumMsgSample"
    "IridiumProcDataSample"
    "IridiumLogSample"
    "EncodeCSourceCode"
    "article"
    "art10"
    "harvard"
    "fancyhdr"
    "floatflt"
    "graphicx")
   (TeX-add-symbols
    '("note" 1)
    '("swj" 1)
    "dds"
    "apex"
    "apf"
    "NComp"
    "sbe"
    "ctd"
    "iridium"
    "WDogTimeOut"
    "mnm"
    "titlename")
   (LaTeX-add-labels
    "sec:toc"
    "sec:tables-n-figures")))

