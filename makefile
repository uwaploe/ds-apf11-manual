.DEFAULT:
.SUFFIXES:
.PRECIOUS: %.aux
.PHONY: all clean force bibtex latex html

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# $Id$
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# RCS Log:
#
# $Log$
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# define the shell to use
SHELL = /bin/csh

# location of the html directory
html-dir = /www/swift

FwRev := $(shell perl -ne 's/.*revision: ([0-9]{6})/\1/g;print' ../src/FirmwareRevision)

all: latex
	@echo Done making $@ ...

clean: clean-latex clean-html
	@echo Done making $@ ...

manual: bibtex dvi
	@echo Done making $@ ...

latex: IridiumApex.tex force #bibtex
	latex IridiumApex

bibtex: IridiumApex.bbl IridiumApex.blg
	@echo Done making $@ ...

ps: IridiumApex.ps
	@echo Done making $@ ...

pdf: IridiumApex.pdf #bibtex
	@echo Done making $@ ...

dvi: IridiumApex.dvi
	@echo Done making $@ ...

IridiumApex.tex: FwRev.tex

%.pdf: %.ps
	ps2pdf $< $@

%.ps: %.dvi
	dvips $< -o $@

%.dvi: %.tex force
	latex $*
	latex $*
	latex $*
	latex $*

%.bbl %.blg: %.aux
	bibtex $*

%.aux: %.tex
	latex $<

clean-latex: force
	-rm -f IridiumApex.{aux,bbl,blg,dvi,lof,log,lot,toc,ps,pdf}
	-rm -f IridiumMsgSample.{aux,bbl,blg,dvi,lof,log,lot,toc,ps,pdf}
	-rm -f IridiumLogSample.{aux,bbl,blg,dvi,lof,log,lot,toc,ps,pdf}
	-rm -f IridiumProcDataSample.{aux,bbl,blg,dvi,lof,log,lot,toc,ps,pdf}
	-rm -f EncodeCSourceCode.{aux,bbl,blg,dvi,lof,log,lot,toc,ps,pdf}
	-rm -f texput.log $(wildcard *.diff)

html: force
	latex2html -address "Dana Swift, swift@ocean.washington.edu" \
	           -dir $(html-dir)/PTV-manual \
              -t "PTV Operating Guidelines" \
              IridiumApex.tex

clean-html: force
	-rm -rf $(html-dir)/IridiumApexManual

FwRev.tex: ../src/FirmwareRevision force
	@perl -i~ -ne 's/\{\\fwrev\}\{[0-9]{6}\}/{\\fwrev}{$(FwRev)}/g;print' FwRev.tex
	@rm -f FwRev.tex~

diff: $(subst .tex,.diff, $(wildcard *.tex))
	@echo Done making $@ ...

difDir = ../../Apf11Sbe41cpBoilerPlate
%.diff: %.tex force
	diff {,$(difDir)/manual/}$< > $@ || true
