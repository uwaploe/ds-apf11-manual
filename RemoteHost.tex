%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% $Id$
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% RCS Log:
%
% $Log$
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\newcommand{\mgetty}{\textbf{mgetty}}
\renewcommand{\-}{\~{\hspace{0in}}}

\section{The remote UNIX host.}
\label{sec:RemoteHost}  

This \iridium\ implementation uses a modem-to-modem communications model.  The
float initiates a telephone call to a remote host computer, logs into the
remote host with a username and password, executes a sequence of commands to
transfer data, and then logs out.  The communications session is float-driven

With respect to the remote host, there is no difference between the float
logging in and a human logging in.  The communications session is initiated
and fully controlled by the float.  On the other hand, the float is not
naturally adaptable or interactive like a human would be and so an unusual
amount of fault tolerance has been built into both sides of the
communications session.

An important fault tolerance measure is redundancy in the form of two
similarly configured remote hosts each with its own dedicated telephone
line.  This is optional but recommended.  Ideally, these two remote hosts
should be separated far enough from each other that power outages or
telephone outages are not likely to simultaneously affect both remote hosts.
The float firmware is designed to automatically switch to the alternate
remote host if with the primary remote host appears to be out of service.

\subsection{System requirements.}
\label{sec:SystemRequirements}

This \iridium\ implementation is strongly tied to the use of a UNIX
computer as the remote host (ie., Microsoft operating systems are not
suitable).  \textbf{The most important ``system requirement'' is a
  system administrator that is familiar, comfortable, and competent in
  a UNIX environment.}  While many different flavors of UNIX could be
made to work, development was done using RedHat and Fedora Linux (all
versions since year 2004).  Fedora Linux (version 21) will be assumed
for the remainder of this section.

\subsubsection{Modem-to-modem (\mnm) communications}
\label{sec:MnM}

Users of small numbers of floats can still use old-fashioned
modem-to-modem (\mnm) communications to receive float data.  For \mnm\
communications, the data pathway is: float \ensuremath{\rightarrow}
Iridium satellite network \ensuremath{\rightarrow} Iridium gateway
(Arizona or Hawaii) \ensuremath{\rightarrow} \emph{Public Switched
  Telephone Network} (PSTN) \ensuremath{\rightarrow} remote host.
Each connection ties-up the phone line for the duration of the call
and so the remote host can service only a single connection for each
available phone line. The \mnm\ communications model scales poorly to
large numbers of floats because banks of phone lines and modems would
be required to service simultaneous connections.

The \mgetty\ package must be installed and configured to monitor a
Hayes-compatible external modem attached to one of the serial ports.  For
information on how to install and configure the \mgetty\ package, refer to
the \mgetty\ documentation supplied with RedHat Linux.  If you customize the
login prompt, make sure that it includes the phrase ``login:''.  Similarly,
make sure that the password prompt includes the phrase ``Password:''.  The
float will not successfully log in if these two phrases are not present.

Once \mgetty\ is installed and configured properly, you should be able to log
into the remote host via a modem-to-modem connection from another computer.
You should test this using the following communications parameters:
4800baud, no parity, 8-bit data, 1 stop-bit.

\subsubsection{RUDICS communications}
\label{sec:rudics}

The RUDICS\footnote{RUDICS: \emph{Router-Based Unrestricted Digital
    Internet Connectivity Solution}} communication model is most
suitable for use by users of large numbers of floats because it solves
the \mnm\ scaling problem referenced in Section~\ref{sec:MnM}.  The
data pathway for rudics communications replaces the PSTN with the
internet: float \ensuremath{\rightarrow} Iridium satellite network
\ensuremath{\rightarrow} Iridium gateway (Arizona or Hawaii)
\ensuremath{\rightarrow} internet \ensuremath{\rightarrow} remote
host.  The remote host can service an arbitrary number of simultaneous
internet connections from the Iridium gateway which eliminates the
need for banks of phone lines and modems.

An open-source \emph{rudicsd} unix server daemon was developed by the
author and freely distributed under the GNU Public License (GPL).  The
\emph{rudicsd} server runs under the xinetd uber-server which renders it
relatively easy for a reasonably competent unix/linux adminstrator to
install, configure, and maintain.  Contact the author for questions or
to obtain the \emph{rudicsd} distribution.

\subsection{Remote host set up.} 
\label{sec:RemoteHostSetUp} 

Once each telemetry cycle, the float downloads ``mission.cfg'' from the home
directory where the float logs in and this new mission configuration becomes
active as the last step before the telemetry cycle terminates (see
Section~\ref{sec:RemoteControl}).  In the context of a UNIX environment,
this simple mechanism allows for great flexibility for remotely controlling
floats individually, in groups, or fleet-wise.  It is also flexible in that
it is possible to switch which model is used even after floats have been
deployed.  Finally, a UNIX-based remote host facilitates easy speciation of
floats as well as for new float developments with no requirement for
backward compatibility.

\subsubsection{Setting up the \emph{default user} on the remote host.}
\label{sec:RemoteHostDefaultUser}

Another fault tolerance measure requires creation of a \emph{default user}
on the remote host.  Begin by creating a new {\sl iridium}\/ group to which
the \emph{default user} and all floats will belong.  As root, execute the
command: 
\begin{quotation}
  {\sl groupadd -g1000 iridium}
\end{quotation}

Next, create an account for the \emph{default user}\/ using \emph{iridium}\/
as the username:
\begin{quotation}
  \mbox{\sl adduser~-s/bin/tcsh~-c"Iridium~Apex~Drifter"~-g"iridium"~-u1000~-d/home/iridium~iridium}
\end{quotation}
Then give the new user a password by executing (as root): 
\begin{quotation}
   {\sl passwd~iridium}
\end{quotation}
For the convenience of the float manager, you might also want to change the
permissions on the float's home directory: 
\begin{quotation}
   {\sl chmod~750~\-iridium}.
\end{quotation}
The file, {\sl /etc/passwd\/}, will contain the following entry:
\begin{verbatim}
   iridium:x:1000:1000:Iridium Apex Drifter:/home/iridium:/bin/tcsh
\end{verbatim}

The remainder of the set-up for this float should be done while logged into
the remote host as the \emph{default user}\/ (ie., \emph{iridium}\/).
Create two directories:
\begin{quotation}
   {\sl mkdir \-/bin \-/logs} 
\end{quotation}
and populate the {\sl \-/bin\/} directory with the \emph{SwiftWare}\/ zmodem
utilities \textbf{rz} and \textbf{sz} as well as the \textbf{chkconfig}
utility.  These three files are in the \textbf{support} directory of your
distribution.

Finally, use \textbf{emacs} to create the following three ascii files:
\textbf{.cshrc}, \textbf{.rzrc}, and \textbf{.szrc}:
\begin{description}
\item[.cshrc:] This file configures the t-shell at login time.  You can
  modify the configuration to suit yourself so long as your customizations
  do not interfere with the effects that the three commands below have.  In
  particular, it is important that the float's \textbf{bin} directory be in
  the {\sl path\/} before any of the system directories.  This will ensure
  that the float's version of the utilities \textbf{chkconfig}, \textbf{rz},
  and \textbf{sz} will be used rather than the system's utilities with these
  same names.
  
  \begin{verbatim}
  # set the hostname
  set hostname=`hostname`
  
  # add directories for local commands
  set path = (. ~/bin /bin /sbin /usr/sbin /usr/local/bin)
  
  # set the prompt
  set prompt=""$hostname":[$cwd]> "
  \end{verbatim}
  
\item[.rzrc:] This is the configuration file for the
  \emph{SwiftWare}\/ implementation the zmodem receive utility.
  \emph{SwiftWare}\/ \textbf{rz} implements the standard zmodem
  protocol but offers configurability and logging facilities that are
  not available with the stock linux zmodem implementation.  The
  \emph{SwiftWare} zmodem distribution is a drop-in replacement for
  the stock linux zmodem package.  Either implementation can be used
  for data transfers from floats but only the \emph{SwiftWare} version
  offers the logging and configuration features referenced below.
  Make sure that the {\sl LogPath}\/ references the \emph{default
    user's}\/ \textbf{logs} directory or else potentially valuable
  logging/debugging information will be irretrievably lost.

  \begin{verbatim}
  # This is the configuration file for 'rz', the 
  # SwiftWare ZModem receive utility.
  
  # specify the serial port device (eg., stdio, /dev/com1)
  ComDev=stdio
  
  # specify the baud rate (ignored if ZModem transfer is via stdio)
  BaudRate=9600
  
  # enable/disable RTS/CTS handshaking (ignored if ZModem transfer is via stdio)
  RtsCtsEnable=1
  
  # enable/disable carrier detection (ignored if ZModem transfer is via stdio)
  CarrierDetectEnable=1
  
  # specify the name of the log file
  LogPath=/net/iridium/logs/rzlog
  
  # enable (AutoLog!=0) or disable (AutoLog==0) the auto-log feature
  AutoLog=1
  
  # enable (CrashRecovery!=0) or disable (CrashRecovery==0) crash-recovery feature
  CrashRecovery=1
  
  # enable or disable crash-recovery requests by sender
  RemoteCrashRecoveryEnable=1
  
  # set the ZModem timeout period (seconds)
  TimeOut=80
  
  # set the delay period (seconds) prior to ZModem session
  ZModemDelay=0
  
  # set the default debug level (range: 0-4)
  DebugBits=0x8005
  \end{verbatim}
 
\item[.szrc:] This is the configuration file for the
  \emph{SwiftWare}\/ implementation the zmodem send utility.
  \emph{SwiftWare}\/ \textbf{sz} implements the standard zmodem
  protocol but offers configurability and logging facilities that are
  not available with the stock linux zmodem implementation.  The
  \emph{SwiftWare} zmodem distribution is a drop-in replacement for
  the stock linux zmodem package.  Either implementation can be used
  for data transfers from floats but only the \emph{SwiftWare} version
  offers the logging and configuration features referenced below.
  Make sure that the {\sl LogPath}\/ references the \emph{default
    user's}\/ \textbf{logs} directory or else potentially valuable
  logging/debugging information will be irretrievably lost.

  \begin{verbatim}
   # This is the confuration file for 'sz', the 
   # SwiftWare ZModem send utility.
   
   # specify the serial port device (eg., stdio, /dev/com1)
   ComDev=stdio
   
   # specify the baud rate (ignored if ZModem transfer is via stdio)
   BaudRate=9600
   
   # enable/disable RTS/CTS handshaking (ignored if ZModem transfer is via stdio)
   RtsCtsEnable=1
   
   # enable/disable carrier detection (ignored if ZModem transfer is via stdio)
   CarrierDetectEnable=1
   
   # specify the name of the log file
   LogPath=/net/iridium/logs/szlog
   
   # enable (AutoLog!=0) or disable (AutoLog==0) the auto-log feature
   AutoLog=1
   
   # enable (CrashRecovery!=0) or disable (CrashRecovery==0) crash-recovery feature
   CrashRecovery=0
   
   # set the ZModem timeout period (seconds)
   TimeOut=80
   
   # set the delay period (seconds) prior to ZModem session
   ZModemDelay=0
   
   # set the default debug level (range: 0-4)
   DebugBits=0x8005
  \end{verbatim}

\end{description}

\subsubsection{Setting up the remote host for individualized remote control.}
\label{sec:RemoteHostUser} 

The ability to individualize each float is implemented by each float having
its own account on the remote host.  The steps to set up the remote host are
analagous to those for setting up the \emph{default user}\/ (see
Section~\ref{sec:RemoteHostDefaultUser}).  For example, to create an account
for float~18047 then make sure the \textbf{iridium} group exists (see
Section~\ref{sec:RemoteHostDefaultUser}) and then execute the following
command (as root):
\begin{quotation}
  \mbox{\sl adduser~-s/bin/tcsh~-c"Iridium~Apex~Drifter"~-g"iridium"~-u118047~-d/home/f18047~f18047}
\end{quotation}
Then give the new user a password and change the permissions of the float's
home directory as shown for the \emph{default user}.  \textbf{Be sure to
  configure the float to use this username and password} (see
Section~\ref{sec:MissionConfiguration}).  The file, \textbf{/etc/passwd},
will contain the following entry:
\begin{verbatim}
   f18047:x:118047:1000:Iridium Apex Drifter:/home/f18047:/bin/tcsh
\end{verbatim}

The remainder of the set-up for this float follows very closely that of the
\emph{default user}\/ and should be done while logged into the remote host
as the float (ie., \emph{f18047}\/).  Create \textbf{bin} and \textbf{logs}
directories in the float's home directory and populate the \textbf{bin}
directory with the \emph{SwiftWare}\/ zmodem utilities \textbf{rz} and
\textbf{sz} as well as the \textbf{chkconfig} utility.

Finally, copy the three ascii files \textbf{.cshrc}, \textbf{.rzrc}, and
\textbf{.szrc} from the \emph{default user's}\/ home directory to the
float's home directory.  Be sure to edit these files so that the {\sl
  LogPath}\/ points to the float's \textbf{logs} directory or else
potentially valuable logging/debugging information will be irretrievably
lost.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "IridiumApex"
%%% End: 
