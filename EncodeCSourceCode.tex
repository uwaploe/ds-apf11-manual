%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% $Id$
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% RCS Log:
%
% $Log$
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\section{Encoding of hydrographic data.}
\label{sec:EncodeCSource}
\renewcommand{\theequation}{\Alph{section}.\arabic{equation}}

The C source code below is used in \apex\ firmware to encode the
hydrographic data before it is telemetered to the remote host.

{\small
\begin{verbatim}
#ifndef ENCODE_H
#define ENCODE_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * $Log$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define encodeChangeLog "$RCSfile$  $Revision$  $Date$"

/* function prototypes */
float DecodeP(unsigned int P);
float DecodeS(unsigned int S);
float DecodeT(unsigned int T);
unsigned char EncodeN(unsigned int NSample);
unsigned int  EncodeP(float p);
unsigned int  EncodeS(float s);
unsigned int  EncodeT(float t);

#endif /* ENCODE_H */
#ifdef ENCODE_C
#undef ENCODE_C

#include <assert.h>
#include <nan.h>

/*------------------------------------------------------------------------*/
/* function to invert the 2-byte encoded pressure                         */
/*------------------------------------------------------------------------*/
/**
   This function inverts the hex-encoding of IEEE-formattted floating point
   pressure data.  The hex-encoding uses 2-byte signed integers with 2's
   complement representation and accounts for the full range of 32-bit IEEE
   floating point values but only values in the open range:
   -3276.7<p<3276.7 are representable.  Any pressure outside this range
   will be represented with a sentinel value.  When decoded, these integer
   sentinels will be represented by floating-point sentinel values as
   described below.


      input:

         P ... The encoded pressure represented as a 2-byte unsigned
               integer.  The encoded pressure satisfies the following
               rules.

            1) Nonfinite values (Inf, -Inf, NaN) are mapped to the sentinel
               hex value: 0x8000.
   
            2) Pressure values less than -3276.7 are mapped to the sentinel
               value: 0x8001.
   
            3) Pressure values greater than 3276.7 are mapped to the
               sentinel value: 0x7fff.
   
            4) All other values are expressed in millibars rounded to the
               nearest integer and expressed as a 16-bit signed integer in
               2's-complement form.

      output:

         p ... The pressure (decibars) expressed as a floating point value.
               The sentinel encoded values 0x7fff, 0x8000, and 0x8001 will
               be mapped to +infinity, NAN, and -infinity, respectively.
*/
float DecodeP(unsigned int P)
{
   /* initialize with the mapping for a nonfinite pressure */
   float p = NaN();
   
   /* check for a valid encoded pressure */
   if (!(P&(~0xffffU)) && P!=0x8000U)
   {
      /* assign out-of-range sentinel values */
      if (P==0x7fffU) p=Inf(+1); else if (P==0x8001U) p=Inf(-1);

      /* invert encoding for negative values */
      else if (P>0x8000U) {P=0x10000L-P; p=-((float)P)/10.0;}

      /* invert encoding for positive values */
      else {p=((float)P)/10.0;}
   }

   return p;
} 

/*------------------------------------------------------------------------*/
/* function to invert the 2-byte encoded salinity                         */
/*------------------------------------------------------------------------*/
/**
   This function inverts the hex-encoding of IEEE-formattted floating point
   salinity data.  The hex-encoding uses 2-byte signed integers with 2's
   complement representation and accounts for the full range of 32-bit IEEE
   floating point values but only values in the open range:
   -4.095<s<61.439 are representable.  Any salinity outside this range
   will be represented with a sentinel value.  When decoded, these integer
   sentinels will be represented by floating-point sentinel values as
   described below.

      input:

         S ... The encoded salinity represented as a 2-byte unsigned
               integer.  The encoded salinity satisfies the following
               rules.

            1) Nonfinite values (Inf, -Inf, NaN) are mapped to the sentinel hex
               value: 0xf000.
      
            2) Salinity values less than -4.095 are mapped to the sentinel
               value: 0xf001.
      
            3) Salinity values greater than 61.439 are mapped to the sentinel
               value: 0xefff.
      
            4) All other values are expressed in parts-per-ten-million
               rounded to the nearest integer and expressed as a 16-bit
               signed integer in 2's-complement form.

      output:

         s ... The salinity (PSU) expressed as a floating point value.
               The sentinel encoded values 0xefff, 0xf000, and 0xf001 will
               be mapped to +infinity, NAN, and -infinity, respectively.
*/
float DecodeS(unsigned int S)
{
   /* initialize with the mapping for a nonfinite salinity */
   float s = NaN();
   
   /* check for a valid encoded salinity */
   if (!(S&(~0xffffU)) && S!=0xf000U)
   {
      /* assign out-of-range sentinel values */
      if (S==0xefffU) s=Inf(+1); else if (S==0xf001U) s=Inf(-1);

      /* invert encoding for negative values */
      else if (S>0xf000U) {S=0x10000L-S; s=-((float)S)/1000.0;}

      /* invert encoding for positive values */
      else {s=((float)S)/1000.0;}
   }

   return s;
}

/*------------------------------------------------------------------------*/
/* function to invert the 2-byte encoded temperature                      */
/*------------------------------------------------------------------------*/
/**
   This function inverts the hex-encoding of IEEE-formattted floating point
   temperature data.  The hex-encoding uses 2-byte signed integers with 2's
   complement representation and accounts for the full range of 32-bit IEEE
   floating point values but only values in the open range:
   -4.095<s<61.439 are representable.  Any temperature outside this range
   will be represented with a sentinel value.  When decoded, these integer
   sentinels will be represented by floating-point sentinel values as
   described below.

      input:

         T ... The encoded temperature represented as a 2-byte unsigned
               integer.  The encoded temperature satisfies the following
               rules.

            1) Nonfinite values (Inf, -Inf, NaN) are mapped to the sentinel hex
               value: 0xf000.
      
            2) Salinity values less than -4.095 are mapped to the sentinel
               value: 0xf001.
      
            3) Salinity values greater than 61.439 are mapped to the sentinel
               value: 0xefff.
      
            4) All other values are expressed in parts-per-ten-million
               rounded to the nearest integer and expressed as a 16-bit
               signed integer in 2's-complement form.

      output:

         t ... The temperature (PSU) expressed as a floating point value.
               The sentinel encoded values 0xefff, 0xf000, and 0xf001 will
               be mapped to +infinity, NAN, and -infinity, respectively.
*/
float DecodeT(unsigned int T)
{
   /* initialize with the mapping for a nonfinite temperature */
   float t = NaN();
   
   /* check for a valid encoded temperature */
   if (!(T&(~0xffffU)) && T!=0xf000U)
   {
      /* assign out-of-range sentinel values */
      if (T==0xefffU) t=Inf(+1); else if (T==0xf001U) t=Inf(-1);

      /* invert encoding for negative values */
      else if (T>0xf000U) {T=0x10000L-T; t=-((float)T)/1000.0;}

      /* invert encoding for positive values */
      else {t=((float)T)/1000.0;}
   }

   return t;
} 

/*------------------------------------------------------------------------*/
/* function to encode the number of samples as an 8-bit unsigned integer  */
/*------------------------------------------------------------------------*/
/**
   This function encodes the number samples in the bin average as an 8-bit
   unsigned integer with protection against overflow.  The encoding accounts
   for the full range of 16-bit unsigned integers but only values in the
   open range: 0<NSample<255 are representable.  This encoding makes full
   use of all 8-bits.

      input:
         NSample ... The number of samples in the bin-average.

      output:
         1) Values greater than or equal to 255 are mapped to 0xff.

         2) All other values are expressed as an 8-bit unsigned integer.
*/
unsigned char EncodeN(unsigned int NSample)
{
   /* prevent overflow of the sample counter */
   unsigned int N = (NSample>=255) ? 0xff : NSample;
   
   return N;
}

/*------------------------------------------------------------------------*/
/* function to encode pressure as a 2-byte unsigned integer               */
/*------------------------------------------------------------------------*/
/**
   This function implements the hex-encoding of IEEE-formattted floating
   point pressure data into 2-byte signed integers with 2's complement
   representation.  The encoding formula accounts for the full range of
   32-bit IEEE floating point values but only values in the open range:
   -3276.7<p<3276.7 are representable.  This encoding makes full use of all
   16-bits.

      input:
         p ... The pressure (decibars) expressed as a floating point value.

      output:
         1) Nonfinite values (Inf, -Inf, NaN) are mapped to the sentinel hex
            value: 0x8000.

         2) Pressure values less than -3276.7 are mapped to the sentinel
            value: 0x8001.

         3) Pressure values greater than 3276.7 are mapped to the sentinel
            value: 0x7fff.

         4) All other values are expressed in millibars rounded to the
            nearest integer and expressed as a 16-bit signed integer in
            2's-complement form.
*/
unsigned int EncodeP(float p)
{
   /* initialize with the mapping for a nonfinite pressure */
   long int P = 0x8000L;

   /* make sure long ints are at least 3 bytes long */
   assert(sizeof(long int)>=3);
   
   if (Finite(p))
   {
      /* assign out-of-range values to sentinel values */
      if (p>=3276.7) P=0x7fffL; else if (p<=-3276.7) P=0x8001L;

      /* encode the pressure as the number of centibars (rounded) */
      else P = (long int)(10*(p + ((p<0) ? -0.05 : 0.05)));

      /* express in 16-bit 2's-complement form */
      if (P<0) P+=0x10000L;
   }

   return P;
} 

/*------------------------------------------------------------------------*/
/* function to encode salinity as a 2-byte unsigned long integer          */
/*------------------------------------------------------------------------*/
/**
   This function implements the hex-encoding of IEEE-formattted floating
   point salinity data into 16-bit unsigned integers with 2's complement
   representation.  The encoding formula accounts for the full range of
   32-bit IEEE floating point values but only values in the open range:
   -4.095<s<61.439 are representable.  This encoding makes full use of all
   16-bits.

      input:
         s ... The salinity (PSU) expressed as a floating point value.

      output:
         1) Nonfinite values (Inf, -Inf, NaN) are mapped to the sentinel hex
            value: 0xf000.

         2) Salinity values less than -4.095 are mapped to the sentinel
            value: 0xf001.

         3) Salinity values greater than 61.439 are mapped to the sentinel
            value: 0xefff.

         4) All other values are expressed in parts-per-ten-million
            rounded to the nearest integer and expressed as a 16-bit
            signed integer in 2's-complement form.
*/
unsigned int EncodeS(float s)
{
   /* initialize with the mapping for a nonfinite salinity */
   long int S = 0xf000L;

   /* make sure that long integers have at least three bytes */
   assert(sizeof(long int)>=3);

   if (Finite(s))
   {
      /* assign out-of-range values to sentinel values */
      if (s>=61.439) S=0xefffL; else if (s<=-4.095) S=0xf001L;

      /* encode the salinity as the number of parts-per-ten-million (rounded) */
      else S = (long int)(1000*(s + ((s<0) ? -0.0005 : 0.0005)));

      /* express in 16-bit 2's-complement form */
      if (S<0) S+=0x10000L;
   }
   
   return S;
}

/*------------------------------------------------------------------------*/
/* function to encode temperature as a 2-byte unsigned integer            */
/*------------------------------------------------------------------------*/
/**
   This function implements the hex-encoding of IEEE-formattted floating
   point temperature data into 16-bit unsigned integers with 2's complement
   representation.  The encoding formula accounts for the full range of
   32-bit IEEE floating point values but only values in the open range:
   -4.095<t<61.439 are representable.  This encoding makes full use of all
   16-bits.

      input:
         t ... The temperature (C) expressed as a floating point value.

      output:
         1) Nonfinite values (Inf, -Inf, NaN) are mapped to the sentinel hex
            value: 0xf000.

         2) Temperature values less than -4.095 are mapped to the sentinel
            value: 0xf001.

         3) Temperature values greater than 61.439 are mapped to the sentinel
            value: 0xefff.

         4) All other values are expressed in tenths of millidegrees Celsius
            rounded to the nearest integer and expressed as a 16-bit signed 
            integer in 2's-complement form.
*/
unsigned int EncodeT(float t)
{
   /* initialize with the mapping for a nonfinite temperature */
   long int T = 0xf000L;

   /* make sure that long integers have at least three bytes */
   assert(sizeof(long int)>=3);

   if (Finite(t))
   {
      /* assign out-of-range values to sentinel values */
      if (t>=61.439) T=0xefffL; else if (t<=-4.095) T=0xf001L;

      /* encode the temperature as the number of tenths of millidegrees (rounded) */
      else T = (long int)(1000*(t + ((t<0) ? -0.0005 : 0.0005)));

      /* express in 16-bit 2's-complement form */
      if (T<0) T+=0x10000L;
   }
   
   return T;
}

#endif /* ENCODE_C */
\end{verbatim}}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "IridiumApex"
%%% End: 
